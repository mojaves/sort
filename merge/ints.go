// (C) 2023 Francesco Romani (I'm fromani on gmail)
// SPDX-License-Identifier: MIT

package merge

func Ints(v []int) []int {
	if len(v) < 2 {
		return v
	}
	x := len(v) / 2
	return mergeInts(Ints(v[:x]), Ints(v[x:]))
}

func IntsNR(v []int) []int {
	if len(v) < 2 {
		return v
	}
	chunks := make([][]int, 0, len(v))
	for n := range v {
		chunks = append(chunks, []int{v[n]})
	}
	A, B := []int{}, []int{}
	for len(chunks) > 1 {
		A, chunks = chunks[0], chunks[1:]
		B, chunks = chunks[0], chunks[1:]
		chunks = append(chunks, mergeInts(A, B))
	}
	return chunks[0]
}

func mergeInts(a, b []int) []int {
	size := len(a) + len(b) // make sure to process all items: we mutate a and b as we go
	r := make([]int, 0, size)
	for k := 0; k < size; k += 1 {
		if len(a) > 0 && len(b) > 0 {
			if a[0] < b[0] {
				r, a = append(r, a[0]), a[1:]
			} else {
				r, b = append(r, b[0]), b[1:]
			}
		} else if len(a) > 0 {
			r = append(r, a...)
			break
		} else { // len(b) > 0
			r = append(r, b...)
			break
		}
	}
	return r
}
