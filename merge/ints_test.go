// (C) 2023 Francesco Romani (I'm fromani on gmail)
// SPDX-License-Identifier: MIT

package merge

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestInts(t *testing.T) {
	type testCase struct {
		name     string
		val      []int
		expected []int
	}

	testcases := []testCase{
		{
			name: "empty",
		},
		{
			name:     "sorted",
			val:      []int{1, 2, 3, 4, 5, 6, 7},
			expected: []int{1, 2, 3, 4, 5, 6, 7},
		},
		{
			name:     "simple",
			val:      []int{7, 2, 3, 1, 5, 6, 4},
			expected: []int{1, 2, 3, 4, 5, 6, 7},
		},
		{
			name:     "random-1",
			val:      []int{16662, 13942, 31258, 25158, 22635, 31199, 9308, 3523, 2421, 606},
			expected: []int{606, 2421, 3523, 9308, 13942, 16662, 22635, 25158, 31199, 31258},
		},
	}

	for _, testcase := range testcases {
		t.Run(testcase.name, func(t *testing.T) {
			got := IntsNR(testcase.val)
			if !reflect.DeepEqual(got, testcase.expected) {
				t.Errorf("got=%v expected=%v", got, testcase.expected)
			}
		})
	}
}

const (
	benchSize = 100000
)

var benchData []int

func init() {
	benchData = make([]int, benchSize, benchSize)
	for idx := 0; idx < benchSize; idx++ {
		benchData[idx] = rand.Int()
	}
}

func BenchmarkInts(b *testing.B) {
	Ints(benchData)
}

func BenchmarkIntsNR(b *testing.B) {
	IntsNR(benchData)
}
